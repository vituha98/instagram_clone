// DO NOT EDIT. This is code generated via package:intl/generate_localized.dart
// This is a library that provides messages for a en_US locale. All the
// messages from the main program should be duplicated here with the same
// function name.

// Ignore issues from commonly used lints in this file.
// ignore_for_file:unnecessary_brace_in_string_interps, unnecessary_new
// ignore_for_file:prefer_single_quotes,comment_references, directives_ordering
// ignore_for_file:annotate_overrides,prefer_generic_function_type_aliases
// ignore_for_file:unused_import, file_names, avoid_escaping_inner_quotes
// ignore_for_file:unnecessary_string_interpolations, unnecessary_string_escapes

import 'package:intl/intl.dart';
import 'package:intl/message_lookup_by_library.dart';

final messages = new MessageLookup();

typedef String MessageIfAbsent(String messageStr, List<dynamic> args);

class MessageLookup extends MessageLookupByLibrary {
  String get localeName => 'en_US';

  final messages = _notInlinedMessages(_notInlinedMessages);
  static Map<String, Function> _notInlinedMessages(_) => <String, Function>{
        "add_post_appbar_post": MessageLookupByLibrary.simpleMessage("Post"),
        "add_post_dialog_cancel":
            MessageLookupByLibrary.simpleMessage("Cancel"),
        "add_post_dialog_choose_from_gallery":
            MessageLookupByLibrary.simpleMessage("Choose from gallery"),
        "add_post_dialog_create_a_post":
            MessageLookupByLibrary.simpleMessage("Create a Post"),
        "add_post_dialog_take_a_photo":
            MessageLookupByLibrary.simpleMessage("Take a photo"),
        "dontHaveAnAccount":
            MessageLookupByLibrary.simpleMessage("Don\'t have an account? "),
        "hint_add_post_description":
            MessageLookupByLibrary.simpleMessage("Write a caption..."),
        "logIn": MessageLookupByLibrary.simpleMessage("Log in"),
        "signUp": MessageLookupByLibrary.simpleMessage("Sign up")
      };
}
