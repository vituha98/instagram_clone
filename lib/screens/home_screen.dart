import 'package:flutter/widgets.dart';
import 'package:instagram_clone/utilis/colors.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: mobileBackgroundColor,
      child: Center(
        child: Text(
          'Home screen mobile',
          style: TextStyle(
            color: primaryColor,
          ),
        ),
      ),
    );
  }
}
